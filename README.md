# Project references

## Papers

### Retrieval based
- [A Web-based Question Answering System](https://pdfs.semanticscholar.org/ad23/647c895d57668fc202259dccbf29edb9e683.pdf)
- [Discriminative Information Retrieval for Question Answering Sentence Selection](https://pdfs.semanticscholar.org/cb25/1429bb45acff3074add06ce20706dfc76e55.pdf)
- [Information Retrieval : Improving Question Answering Systems by Query Reformulation and Answer Validation](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.193.1603&rep=rep1&type=pdf)
- [Retrieval Using Structure for Question Answering](https://www.cs.cmu.edu/~pto/papers/TDM_2004_XML_QA.pdf)
- [Retrieval-based Question Answering for Machine Reading Evaluation](https://pdfs.semanticscholar.org/f9bc/e3e9f8d4916e25eec79610280c9b21153f24.pdf)
- [Scaling Question Answering to the Web](https://homes.cs.washington.edu/~weld/papers/mulder-www10.pdf)
- [Structured Retrieval for Question Answering](https://www.cs.cmu.edu/~callan/Papers/sigir07-mbilotti.pdf)
- [Answer Extraction and Passage Retrieval for Question Answering Systems](http://ijarcet.org/wp-content/uploads/IJARCET-VOL-5-ISSUE-12-2703-2706.pdf)
- [Open Domain Question Answering via Semantic Enrichment](http://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/frp1068-sunA.pdf)

### Neural network based
- [Building End-to-End Dialogue Systems Using Generative Hierarchical Neural Network Models](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwjOrPS1u7rcAhVO0RoKHYdjBj0QFgg5MAE&url=https%3A%2F%2Fwww.aaai.org%2Focs%2Findex.php%2FAAAI%2FAAAI16%2Fpaper%2Fdownload%2F11957%2F12160&usg=AOvVaw0By8LYzTXQam7QAPTGZvJT)
- [Dialogue Response Generation using Neural Networks with Attention and Background Knowledge](http://jens-lehmann.org/files/2017/cscubs_dialogues.pdf)
- [LSTM-based Deep Learning Models for Non-factoid Answer Selection](https://arxiv.org/abs/1511.04108)
- [Neural Generative Question Answering](https://arxiv.org/abs/1512.01337)
- [Neural Responding Machine for Short-Text Conversation](https://arxiv.org/abs/1503.02364)
- [Question Answering Using Deep Learning](https://cs224d.stanford.edu/reports/StrohMathur.pdf)